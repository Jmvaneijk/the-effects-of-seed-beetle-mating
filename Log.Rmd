---
title: "Log"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

11 - 02- 2020

Today We started our project! I studied the PowerPoint and read chapter 1 + 2 of the manual. Afterwards I started looking for 
data that will be used for the project. when the dataset was found, the project proposal was started.

---------------

25 - 02 - 2020

The goal for today was to finish the project proposal and load the dataset into R. 
The proposal was finished and the dataset was loaded into R.
A few small adjustments were made and the result was a data-frame.

---------------

27 - 02 - 2020

The project proposal was submitted, chapter 3 was studied and I started on the visualazation. I ended at chapter
3.5.5

---------------

05 - 03 - 2020

The goal for today is to finish chapter 3 and start working on chapter 4. 
Chapter 3 has been finished and the data has been examined.
Work on chapter 4 has also been started. chapter 4.1 and 4.2 have been studied. 
currently the FPM normalization has been completed 
and now the fold change needs to be calculated.

---------------

12 - 03 - 2020

The goal for today is to finish the log2-FC histogram and start working on the statistical tests. 
The histogram has been finished and 
the next step is to do the ANOVA testing.

---------------

13 - 03 - 2020

The goal for today is to finish chapter 4. 
Part of chapter 4 has been completed. the first question has been answered and some plots have been made 
with the output data. The second question still needs to be answered and some bugs need to be fixed. This will be done next time!

---------------

19 - 03 - 2020

Today I am going to continue working on chapter 4. Before this however, I integrated the given tips into the code. 
The heatmap, for example, has been fixed now,
more comments have been added, among some other changes. The GLM-treating sections have been removed and the second question has been answered.

---------------

20 - 03 - 2020

Today I made some more changes to the chapter four, and I started working on chapter 5. I made some threshold data and started making some 
venn diagrams. The results however, seem a little confusing to me, and do not match the article results at all. I also have a lot of results and 
dataframes which makes me a little doubtful if I'm still doing everything right. So for the next time I plan on double checking everything
and figuring out if I am actually going into the right direction. 
If there is time and everything is okay, I can start working on more visualization.

---------------

25 - 03 - 2020

Today i looked a little more into the data to find out why the results weren't same. 
After that I created the thresholds for all the edge R results, and
made the first 4 volcano plots. The goal for tomorrow is to finish all these plots and add explanations.

---------------

26 - 03 - 2020

A custom function that creates the volcano plots has been made, saving a lot of repetitive code. The volcano plots have all been made and plotted too, 
using this function. The venn-diagrams have also been updated, with them looking a little more realistic. 
There has also been a bit of work on the Read-me for the repo.

---------------

02 - 04 - 2020

No new content has been added today, however the whole code got a rework instead. Since there is a lot of repeated use of the same commands, and I
liked the concept of the volcano plot function. I decided to make functions for all the big steps and reduce the redundancy of the code.
Some of the comments have also been updated/changed/added. 
The plan for tomorrow is to do some new plots and maybe draw the final conclusion, leaving the final week open for last minute fixes and improvements!

---------------

03 - 04 - 2020

For now I decided not to make more plots or figures and focus on the final conclusion, which has been finished. This leaves next week open to 
fixes etc. and maybe more plots. 

I decided not to make more plots because I didn't think they would add more to the overall results and research.

---------------

06 - 04 - 2020

Doing spelling/grammar checks to hopefully filter out most of those mistakes, Doing some bug testing and just
some more general style changes.
The readme had also been updated, and now contains the latest info.

---------------

10 - 04 - 2020

The final check done + repo prepared