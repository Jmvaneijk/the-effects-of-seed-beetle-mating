# README #

The global explanation of all the files and folders on this repo.

## [Proposal](https://bitbucket.org/Jmvaneijk/thema_7/src/master/Proposal/) ##

This folder contains the latest and submitted version of the project proposal. 
With both the original .docx and.pdf versions of the file.

## [Supp_data](https://bitbucket.org/Jmvaneijk/thema_7/src/master/Supp_data/) ##

Folder containing all the original results from the publishers which could be used
to check your own results, or to replicate their exact figures.

## [Data](https://bitbucket.org/Jmvaneijk/thema_7/src/master/data/)  ##

The count data used for the research in both the .txt and .csv format.

## The Front Page ##

The main .Rmd code can be found here.
The log can also be found here, describing everything that was done at which time.
The PDF files for both the log and the results can be found on this page too.
The final thing found on this page is the readme, which is also shown below.